
.PHONY:
init: build up shell

.PHONY:
build:
	docker-compose build

.PHONY:
up:
	docker-compose up -d

.PHONY:
shell:
	docker exec -it merger-app bash

.PHONY:
down:
	docker-compose down
