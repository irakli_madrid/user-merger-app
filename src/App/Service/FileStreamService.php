<?php

namespace App\Service;

class FileStreamService
{
    /**
     * Reads content from a file and returns an array.
     *
     * @param string $filename
     * @return array
     */
    public function getContent(string $filename): array
    {
        $data = [];
        $file = fopen($filename, 'r');
        while ($line = fgets($file)) {
            $data[] = explode(',', $line);
        }
        fclose($file);

        return $data;
    }
}
