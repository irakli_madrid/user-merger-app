<?php

namespace App;

use App\Parser\CsvParser;
use App\Parser\JsonParser;
use App\Parser\XmlParser;

class Parser
{
    public function __construct(
        private CsvParser $csvParser,
        private JsonParser $jsonParser,
        private XmlParser $xmlParser
    ) {}

    public function getParsedContent(string $filename): array
    {
        $extension = explode('.', $filename);
        $extension = end($extension);

        return match ($extension) {
            'csv' => $this->csvParser->parse($filename),
            'json' => $this->jsonParser->parse($filename),
            'xml' => $this->xmlParser->parse($filename),
            default => throw new \Exception(sprintf('"%s" extension is not supported!', $extension)),
        };
    }
}
