<?php

namespace App\Parser;

class JsonParser extends BaseParser
{
    public function parse(string $filename): array
    {
        $content = file_get_contents($filename);
        $data = json_decode($content, true);

        $result = [];
        foreach ($data as $row) {
            $result[$row['user_id']] = [
                'id' => (string) $row['user_id'],
                'firstname' => $row['first_name'],
                'lastname' => $row['last_name'],
                'username' => $row['username'],
                'type' => $row['user_type'],
                'last_login' => $this->formatTime($row['last_login_time']),
            ];
        }

        return $result;
    }
}
