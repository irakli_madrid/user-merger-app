<?php

namespace App\Parser;

interface ParserInterface
{
    public function parse(string $filename): array;
}
