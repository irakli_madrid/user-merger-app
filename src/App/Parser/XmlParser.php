<?php

namespace App\Parser;

class XmlParser extends BaseParser
{
    public function parse(string $filename): array
    {
        $xmlContent = simplexml_load_file($filename);

        $data = json_decode(json_encode($xmlContent), true)['user'];

        $result = [];
        foreach ($data as $row) {
            $result[$row['userid']] = [
                'id' => $row['userid'],
                'firstname' => $row['firstname'],
                'lastname' => $row['surname'],
                'username' => $row['username'],
                'type' => $row['type'],
                'last_login' => $this->formatTime($row['lastlogintime']),
            ];
        }

        return $result;
    }
}
