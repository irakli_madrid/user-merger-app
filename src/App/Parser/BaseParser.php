<?php

namespace App\Parser;

abstract class BaseParser implements ParserInterface
{
    protected function formatTime(string $stringFormat): string
    {
        $stringFormat = trim($stringFormat);
        $dateTimeObject = new \DateTime($stringFormat);
        return $dateTimeObject->format(\DateTime::ISO8601);
    }
}
