<?php

namespace App\Parser;

use App\Service\FileStreamService;

class CsvParser extends BaseParser
{
    public function __construct(
        private FileStreamService $fileStreamService,
    ) {}

    public function parse(string $filename): array
    {
        $data = $this->fileStreamService->getContent($filename);

        return $this->formatData($data);
    }

    /**
     * Adds column names to data.
     *
     * @param array $data
     * @return array
     */
    private function formatData(array $data): array
    {
        $result = [];

        unset($data[0]);
        for ($i = 1; $i < count($data) + 1; $i++) {
            $row = $data[$i];
            $result[$row[0]] = [
                'id' => trim($row[0]),
                'firstname' => trim($row[1]),
                'lastname' => trim($row[2]),
                'username' => trim($row[3]),
                'type' => trim($row[4]),
                'last_login' => $this->formatTime($row[5]),
            ];
        }

        return $result;
    }
}
