<?php

require_once __DIR__.'/vendor/autoload.php';

use App\Parser;

$container = new DI\Container();
$parser = $container->get(Parser::class);

$inputs = [
    'input/users.csv',
    'input/users.json',
    'input/users.xml',
];
$data = [];
foreach ($inputs as $filename) {
    $data = array_replace($data, $parser->getParsedContent($filename));
}
ksort($data);

$json = json_encode(array_values($data), JSON_PRETTY_PRINT);
file_put_contents('output/users2.json', $json . PHP_EOL);

echo "Content generated successfully: output/users2.json!\n";
