<?php

use PHPUnit\Framework\TestCase;

use App\Parser\CsvParser;
use App\Service\FileStreamService;

final class CsvParserTest extends TestCase
{
    private $parser;

    public function setUp(): void
    {
        $fileStreamServce = $this->createMock(FileStreamService::class);
        $fileStreamServce->expects($this->any())
            ->method('getContent')
            ->willReturn([
                [
                    'User ID',
                    'First Name',
                    'Last Name',
                    'Username',
                    'User Type',
                    'Last Login Time',
                ],
                [
                    3,
                    'David',
                    'Payne',
                    'Dpayne',
                    'Manager',
                    '23-09-2014 09:35:02',
                ],
                [
                    10,
                    'Ruby',
                    'Wax',
                    'ruby',
                    'Employee',
                    '12-12-2014 08:09:13',
                ],
            ]);

        $this->parser = new CsvParser($fileStreamServce);
    }

    public function testParse()
    {
        $expected = [
            3 => [
                'id' => '3',
                'firstname' => 'David',
                'lastname' => 'Payne',
                'username' => 'Dpayne',
                'type' => 'Manager',
                'last_login' => '2014-09-23T09:35:02+0000',
            ],
            10 => [
                'id' => '10',
                'firstname' => 'Ruby',
                'lastname' => 'Wax',
                'username' => 'ruby',
                'type' => 'Employee',
                'last_login' => '2014-12-12T08:09:13+0000',
            ],
        ];
        $data = $this->parser->parse('filename.csv');
        $this->assertSame($expected, $data);
    }
}
